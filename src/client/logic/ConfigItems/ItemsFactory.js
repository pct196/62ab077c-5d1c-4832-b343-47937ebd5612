import { ref } from 'vue';
import { v4 } from 'uuid';

const ITEM_TYPES = Object.freeze({
	ONE: 'ONE',
	TWO: 'TWO'
});

class BaseItem {
  constructor(component, idx) {
    this.component = component;
		this.counter = ref(0);
		this.idx = ref(idx);

		const itemId = v4();
		this.itemId = ref(itemId);
  }

  decrement() { this.counter.value--; }
  increment() { this.counter.value++; }
}

class One extends BaseItem {
	constructor(idx) {
		super('OneUI', idx)
	}
}

class Two extends BaseItem {
	constructor(idx) {
		super('TwoUI', idx)
	}

  decrement() { this.counter.value -= 2; }
  increment() { this.counter.value += 2; }
}

const ItemFactory = (type, idx) => {
	if (!Object.keys(ITEM_TYPES).includes(type)) throw new Error('Invalid Item');

	let item = null;
	switch (type) {
		case ITEM_TYPES.ONE:{
			item = new One(idx);
			break;
		}
		case ITEM_TYPES.TWO:{
			item = new Two(idx);
			break;
		}
	
		default:
			break;
	}

	return item;
};

export { ITEM_TYPES, ItemFactory }