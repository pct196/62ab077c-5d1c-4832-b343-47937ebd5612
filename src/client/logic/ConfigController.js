import { computed, shallowReactive } from 'vue';
import { ITEM_TYPES, ItemFactory } from './ConfigItems/ItemsFactory';

class ConfigController {
  constructor() {
    // this.items = shallowReactive(Array.from({ length: 5 }, (item, idx) => new Item(idx)));
    this.items = shallowReactive([
			ItemFactory(ITEM_TYPES.ONE, 0),
			ItemFactory(ITEM_TYPES.TWO, 1)
		]);
  }

  addItem(type = ITEM_TYPES.ONE) {
    this.items.push(ItemFactory(type, this.items.length));
  }

	get sum() {
		return computed(() => this.items.reduce((total, item) => total += item.counter.value, 0));
	}
}

export { ConfigController };
